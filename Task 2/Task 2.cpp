﻿//a3 = a % 10;
//a2 = ((a % 100) - a3) / 10;
//a1 = a / 100

#include <stdio.h>
#include <math.h>
#include <windows.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int a, a1,a2,a3, i;
	do
	{
		system("cls");
		printf("Введіть ціле та додатнє трьохзначне число.\n");
		printf("->");
		scanf_s("%d", &a);
		if (99 < a && a < 1000)
		{
			a3 = a % 10;
			a2 = ((a % 100) - a3) / 10;
			a1 = a / 100;
			printf("Нове число -> %d%d%d\n\n", a2, a1, a3);
		}
		else printf("Помилка, число не входить у потрібний діапазон\n");
	printf("Продовжити роботу - 1\nЗакінчити роботу - 2\n");
	scanf_s("%d", &i);
	} while (i != 2);
	return 0;
}