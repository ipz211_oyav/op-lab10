﻿#include <stdio.h>
#include <windows.h>
#include <math.h>

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int n, s=0, i, k=0;
	do 
	{
		system("cls");
		printf("Введіть потрібні числа. Число 0 закінчує обчислення.\n");
		do
		{
			printf("->"); scanf_s("%d", &n);
			if (abs(n) % 2 == 0)
				s += n;
		} while (n != 0);
		printf("s = %d\n\n", s);
		Sleep(500);
		printf("Продовжити роботу - 1\nЗакінчити роботу - 2\n");
		scanf_s("%d", &i);
	} 
	while (i != 2);
	return 0;
}
